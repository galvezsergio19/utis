User Time-In System v.1 (UTIS)

Description: A system that acts like a biometrics does. With real-time logging, this system makes it possible to record the time-in and time-out of a user's attendance with ease. It provides a user-friendly UI and shortcut keys and for fast processing of records and logging. It also has a picture for each user to ensure user's identity when being logged. This app is good for school and office wherein they maintain a huge record for attendance. Hope you all like this system :)

LOGIN ACCOUNT: Shown at the front-end of system
Account Number: 11-0127
Password: 1

ADMIN ACCESS: Back-end of system
Username: admin
Password: admin

Note: Some issues were not yet fixed thus I will be updating this on several upcoming weeks / months. And I hope that this project will be used for educational purposes and that the system itself maintains its copyrights as proposed.
